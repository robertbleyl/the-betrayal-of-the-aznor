extends StateMachine.State
class_name EnemyChaseState

const ID = "CHASE"

@onready var nav_agent: NavigationAgent3D = %NavigationAgent3D

var _player_character: PlayerCharacter
var _data: Dictionary

func _get_id() -> String:
	return ID

func _activate(data: Dictionary):
	super._activate(data)
	_data = data
	_player_character = data["player_character"]

func _physics_process_state(_delta: float):
	nav_agent.target_position = _player_character.global_position

func _on_attack_range_body_entered(_body: Node3D):
	if _is_active:
		transition_to_state.emit(EnemyAttackState.ID, _data)
		_deactivate()

func _on_player_detection_range_body_exited(_body: Node3D):
	if _is_active:
		transition_to_state.emit(EnemyIdleState.ID, {})
		_deactivate()
