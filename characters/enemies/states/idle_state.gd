extends StateMachine.State
class_name EnemyIdleState

const ID = "IDLE"

@onready var nav_agent: NavigationAgent3D = %NavigationAgent3D
@onready var player_detection_sound: DisposableAudioEmitter = %PlayerDetectedSound

func _get_id() -> String:
	return ID

func _activate(_data: Dictionary):
	super._activate(_data)
	
	nav_agent.target_position = global_position
	nav_agent.velocity = Vector3.ZERO

func _on_player_detection_area_body_entered(body: Node3D):
	if _is_active:
		var data: Dictionary = {}
		data["player_character"] = body
		transition_to_state.emit(EnemyChaseState.ID, data)
		player_detection_sound.play_copy()
		_deactivate()
