extends StateMachine.State
class_name EnemyAttackState

const ID = "ATTACK"

@onready var nav_agent: NavigationAgent3D = %NavigationAgent3D
@onready var weapon: Weapon = %Weapon

var _player_character: PlayerCharacter
var _data: Dictionary

func _get_id() -> String:
	return ID

func _activate(data: Dictionary):
	super._activate(data)
	_data = data
	_player_character = data["player_character"]
	call_deferred("_stop_movement")
	
func _stop_movement():
	nav_agent.target_position = global_position
	nav_agent.velocity = Vector3.ZERO

func _process_state(_delta: float):
	if weapon != null:
		weapon.fire(_player_character.global_position)

func _on_attack_range_body_exited(_body: Node3D):
	if _is_active:
		transition_to_state.emit(EnemyChaseState.ID, _data)
		_deactivate()
