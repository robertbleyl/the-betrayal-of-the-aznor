extends RigidBody3D
class_name Enemy

@onready var nav_agent: NavigationAgent3D = %NavigationAgent3D
@onready var health: NumberProperty = %Health
@onready var state_machine: StateMachine = %StateMachine
@onready var weapon: Weapon = %Weapon
@onready var health_bar: ProgressBar = %HealthBar
@onready var bullet_hit_armor_sound: DisposableAudioEmitter = %BulletHitArmorSound
@onready var item_hit_armor_sound: DisposableAudioEmitter = %ItemHitArmorSound
@onready var item_hit_sound: DisposableAudioEmitter = %ItemHitSound

var _first_frame_done: bool
var _armor_pieces: Array[Node]

var _health_bar_offset: Vector2

func _ready():
	nav_agent.velocity_computed.connect(_on_nav_velocity_computed)
	_armor_pieces = find_children("ArmorBase*", "", true, false)
	weapon.equip(self)
	
	health_bar.max_value = health.max_value
	health_bar.value = health._current_value
	_health_bar_offset = health_bar.global_position

func receive_damage(event: DamageEvent):
	if _armor_pieces.is_empty():
		item_hit_sound.play_copy()
		health.decrease(event.damage_amount)
	elif event.collider is Item:
		item_hit_armor_sound.play_copy()
		call_deferred("_remove_armor", event)
	elif event.collider is ProjectileBase:
		bullet_hit_armor_sound.play_copy()

func _remove_armor(event: DamageEvent):
	var piece: Node = _armor_pieces.pick_random()
	piece.get_parent().remove_child(piece)
	get_parent().add_child(piece)
	piece.global_position = global_position
	piece.apply_central_impulse(event.direction)
	piece.state_machine.transition_to_state(IdleState.ID, {})
	_armor_pieces.erase(piece)

func _on_health_changed(new_amount: float):
	health_bar.value = new_amount
	
	if new_amount <= 0:
		call_deferred("_die")

func _die():
	if weapon != null:
		remove_child(weapon)
		get_parent().add_child(weapon)

		weapon.state_machine.transition_to_state(IdleState.ID, {})
		weapon.set_collision_layer_value(CollisionLayers.FLAT_ITEMS, true)
		weapon.global_position = global_position
	
	queue_free()
	
func _on_nav_velocity_computed(safe_velocity: Vector3):
	linear_velocity = safe_velocity

func _process(_delta: float):
	var cam = get_viewport().get_camera_3d()
	health_bar.global_position = cam.unproject_position(global_position) + _health_bar_offset
	health_bar.rotation = 0

func _physics_process(_delta: float):
	if !_first_frame_done:
		_first_frame_done = true;
		return
	
	if nav_agent.is_navigation_finished():
		return
	
	var next_pos = nav_agent.get_next_path_position()
	var dir = next_pos - global_position
	var new_velocity = dir.normalized() * nav_agent.max_speed
	nav_agent.velocity = new_velocity

	look_at(nav_agent.target_position)
