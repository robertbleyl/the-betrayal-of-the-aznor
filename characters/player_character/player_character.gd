extends RigidBody3D
class_name PlayerCharacter

@onready var item_catch_shape: CollisionShape3D = %ItemCatchCircle
@onready var health: NumberProperty = %Health
@onready var equip_weapon_sound: DisposableAudioEmitter = %EquipWeaponSound

@export var cam_mouse_offset_factor_max: float = 0.66
@export var cam_mouse_offset_factor_scaling: float = 50.0
@export var cam_height: float = 20.0
@export var move_speed: float = 400.0
@export var gamepad_cursor_speed: float = 15.0

var _move_direction: Vector3
var _current_item: Item
var _equipped_weapon: Weapon

func _unhandled_input(_event: InputEvent):
	if Input.is_action_just_pressed('Equip and Unequip Weapon'):
		if _current_item != null && _current_item is Weapon && _equipped_weapon == null:
			_equipped_weapon = _current_item
			_equipped_weapon.equip(self)
	
			equip_weapon_sound.play_copy()
			
			_equipped_weapon.position = Vector3.ZERO
			_current_item = null
		elif _current_item == null && _equipped_weapon != null:
			_current_item = _equipped_weapon
			_current_item.catch(self)
			_set_caught_item_position(_current_item)
			_equipped_weapon = null

func _process(_delta: float):
	var input_vector = Input.get_vector('Move Left', 'Move Right', 'Move Up', 'Move Down')
	_move_direction = Vector3(input_vector.x, 0, input_vector.y)
	_update_camera_position()

	if Input.is_action_pressed("Pick Up Item") && _current_item == null:
		_try_to_pick_up_item()
	elif Input.is_action_just_pressed("Pick Up Item") && _current_item != null:
		_throw_item()

	if _equipped_weapon != null && Input.is_action_pressed("Fire Weapon"):
		var mouse_pos_screen = get_viewport().get_mouse_position()
		var cam = get_viewport().get_camera_3d()
		_equipped_weapon.fire(cam.project_position(mouse_pos_screen, cam_height))

	if !Input.get_connected_joypads().is_empty():
		var cam_movement = Input.get_vector(
			'Move Camera Left',
			'Move Camera Right',
			'Move Camera Up',
			'Move Camera Down'
		)
		get_viewport().warp_mouse(get_viewport().get_mouse_position() + cam_movement * gamepad_cursor_speed)


func _update_camera_position():
	var cam = get_viewport().get_camera_3d()
	var mouse_pos_screen = get_viewport().get_mouse_position()

	var viewport_size = get_viewport().get_visible_rect().size
	var max_screen_x = viewport_size.x * cam_mouse_offset_factor_max
	var max_screen_y = viewport_size.y * cam_mouse_offset_factor_max
	
	var player_pos_screen = cam.unproject_position(global_position)

	var offset_x = _get_offset_coord(max_screen_x, mouse_pos_screen.x, player_pos_screen.x)
	var offset_z = _get_offset_coord(max_screen_y, mouse_pos_screen.y, player_pos_screen.y)

	cam.global_position = global_position + Vector3(offset_x, cam_height, offset_z)


func _get_offset_coord(max_val: float, mouse_coord: float, player_coord: float) -> float:
	return minf(max_val, mouse_coord - player_coord) / cam_mouse_offset_factor_scaling


func _try_to_pick_up_item():
	var cam = get_viewport().get_camera_3d()
	var space_state = get_world_3d().direct_space_state
	var mouse_pos = get_viewport().get_mouse_position()
	var query = PhysicsRayQueryParameters3D.new()
	query.from = cam.position
	query.to = cam.project_position(mouse_pos, cam_height + 1)
	query.collision_mask = 0b1010011000
	
	var result = space_state.intersect_ray(query)
	
	if result.is_empty():
		return
	
	var item: Item = result.collider
	
	if !item.is_state(ThrowState.ID) && !item.is_state(EquipArmorState.ID):
		_current_item = item
		_current_item.pick_up(self)

func _throw_item():
	var state: String = _current_item.state_machine._current_state_id
	
	if state != FetchState.ID && state != CaughtState.ID:
		return
	
	var cam = get_viewport().get_camera_3d()
	var mouse_pos = get_viewport().get_mouse_position()
	var direction = cam.project_position(mouse_pos, cam_height - 1) - _current_item.global_position
	_current_item.throw(direction.normalized())
	_current_item = null

func receive_damage(event: DamageEvent):
	health.decrease(event.damage_amount)

func _on_health_changed(new_amount: float):
	if new_amount <= 0:
		# TODO death animation
		queue_free()

func _physics_process(delta: float):
	linear_velocity = _move_direction * move_speed * delta
	
	var cam = get_viewport().get_camera_3d()
	look_at(cam.project_position(get_viewport().get_mouse_position(), cam_height))

func _on_item_catch_area_body_entered(body: Node3D):
	if body is Item:
		var item: Item = body
		
		if item.state_machine._current_state_id == FetchState.ID:
			item.catch(self)
			_set_caught_item_position(item)

func _set_caught_item_position(item: Item):
	var item_size = item.collision_shape.shape.size.length() / 2
	item.position = Vector3.RIGHT * (item_catch_shape.shape.radius - item_size)
