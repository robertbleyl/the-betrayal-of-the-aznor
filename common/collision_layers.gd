extends Node
class_name CollisionLayers

const NAVIGATION = 1
const ENEMIES = 2
const PLAYER = 3
const LIGHT_ITEMS = 4
const HEAVY_ITEMS = 5
const FETCHING_ITEMS = 6
const CAUGHT_ITEMS = 7
const THROWING_ITEMS = 8
const PROJECTILES = 9
const FLAT_ITEMS = 10
