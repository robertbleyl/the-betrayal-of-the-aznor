extends Node
class_name NumberProperty

@export var max_value: float = 100

signal changed(new_value: float)

var _current_value: float

func _ready():
	_current_value = max_value

func increase(amount: float):
	_current_value += amount
	changed.emit(_current_value)

func decrease(amount: float):
	_current_value -= amount
	changed.emit(_current_value)
