extends Node3D
class_name StateMachine

@export var initial_state_id: String

var _states: Dictionary = {}
var _current_state_id: String

signal state_changed(state_id: String)

func _ready():
	var states: Array[Node] = find_children("*State", "", true, false)
	
	for state: State in states:
		_states[state._get_id()] = state
		state.transition_to_state.connect(transition_to_state)
	
	transition_to_state(initial_state_id, {})

func transition_to_state(state_id: String, data: Dictionary):
	if _current_state_id:
		var current_state: State = _states[_current_state_id]
		current_state._deactivate()
	
	_current_state_id = state_id
	var state: State = _states[_current_state_id]
	state._activate(data)
	
	state_changed.emit(state_id)

func is_state(state_id: String) -> bool:
	return _current_state_id == state_id

class State extends Node3D:
	
	signal transition_to_state(state_id: String, data: Dictionary)
	
	var _is_active: bool
	
	func _get_id() -> String:
		return ""
	
	func _activate(_data: Dictionary):
		_is_active = true
	
	func _deactivate():
		_is_active = false
	
	func _process(delta: float):
		if _is_active:
			_process_state(delta)

	func _process_state(_delta: float):
		pass
	
	func _physics_process(delta: float):
		if _is_active:
			_physics_process_state(delta)

	func _physics_process_state(_delta: float):
		pass
