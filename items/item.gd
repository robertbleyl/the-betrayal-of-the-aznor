extends RigidBody3D
class_name Item

@onready var state_machine: StateMachine = %StateMachine
@onready var collision_shape: CollisionShape3D = %CollisionShape3D
@onready var health: NumberProperty = %Health
@onready var mesh: MeshInstance3D = %Mesh

@export var collision_damage: float = 10
@export var destruction_particle_scene: PackedScene

var _previous_velocity: Vector3

func pick_up(player_character: PlayerCharacter):
	var data: Dictionary = {}
	data["player_character"] = player_character
	state_machine.transition_to_state(PickUpState.ID, data)

func catch(player_character: PlayerCharacter):
	var data: Dictionary = {}
	data["player_character"] = player_character
	state_machine.transition_to_state(CaughtState.ID, data)

func throw(direction: Vector3):
	var data: Dictionary = {}
	data["direction"] = direction
	state_machine.transition_to_state(ThrowState.ID, data)

func _physics_process(_delta: float):
	_previous_velocity = linear_velocity

func receive_damage(event: DamageEvent):
	health.decrease(event.damage_amount)

	if health._current_value <= 0:
		call_deferred("_destroy", event.direction)

func _destroy(direction: Vector3):
	var particles: GPUParticles3D = destruction_particle_scene.instantiate()
	particles.global_position = global_position
	particles.process_material.direction = Vector3(direction.x, direction.y, 0)
	get_parent().add_child(particles)
	queue_free()

func is_state(state_id: String) -> bool:
	return state_machine.is_state(state_id)

func _on_body_entered(body: Node3D):
	if is_state(ThrowState.ID) || is_state(FetchState.ID):
		var direction = _previous_velocity
		var event: DamageEvent = DamageEvent.new()
		event.damage_amount = 1
		event.direction = direction
		receive_damage(event)
		
		if !body is Item && body.has_method("receive_damage"):
			var evt: DamageEvent = DamageEvent.new()
			evt.damage_amount = collision_damage
			evt.direction = direction
			evt.collider = self
			body.receive_damage(evt)

func _on_mouse_entered():
	if !is_state(CaughtState.ID) && !is_state(EquipState.ID) && !is_state(EquipArmorState.ID):
		mesh.get_active_material(0).emission_enabled = true

func _on_mouse_exited():
		mesh.get_active_material(0).emission_enabled = false
