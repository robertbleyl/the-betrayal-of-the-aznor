extends StateMachine.State
class_name FetchState

const ID = "FETCH"

@export var speed: float = 1000000.0

var _item: Item
var _player_character: PlayerCharacter

func _get_id() -> String:
	return ID

func _activate(data: Dictionary):
	super._activate(data)
	_player_character = data["player_character"]
	
	_item = get_parent().get_parent()
	_item.freeze = false
	
	_item.set_collision_layer_value(CollisionLayers.LIGHT_ITEMS, false)
	_item.set_collision_layer_value(CollisionLayers.HEAVY_ITEMS, false)
	_item.set_collision_layer_value(CollisionLayers.FETCHING_ITEMS, true)
	
	_item.set_collision_mask_value(CollisionLayers.PLAYER, false)
	_item.set_collision_mask_value(CollisionLayers.LIGHT_ITEMS, false)
	_item.set_collision_mask_value(CollisionLayers.HEAVY_ITEMS, false)

func _process_state(delta: float):
	if _player_character == null:
		return
	
	var force = _player_character.global_position - _item.global_position
	force = force.normalized() * delta * speed
	_item.apply_central_force(force)
