extends StateMachine.State
class_name ThrowState

const ID = "THROW"

@onready var sound: DisposableAudioEmitter = %ThrowSound

@export var speed: float = 10000.0

var _item: Item
var _direction: Vector3

func _get_id() -> String:
	return ID

func _activate(_data: Dictionary):
	super._activate(_data)
	_direction = _data["direction"]
	
	_item = get_parent().get_parent()
	_item.set_collision_mask_value(CollisionLayers.LIGHT_ITEMS, true)
	_item.set_collision_mask_value(CollisionLayers.HEAVY_ITEMS, true)
	_item.set_collision_layer_value(CollisionLayers.FETCHING_ITEMS, false)
	_item.set_collision_layer_value(CollisionLayers.THROWING_ITEMS, true)
	
	call_deferred("_throw")

func _throw():
	var prev_global_pos = _item.global_position
	_item.freeze = false
	
	var world = _item.get_parent().get_parent()
	if world != null:
		_item.get_parent().remove_child(_item)
		world.add_child(_item)
		_item.position = prev_global_pos
	
	var impulse = _direction * speed
	_item.apply_central_impulse(impulse)
	
	sound.play_copy()

func _on_item_base_body_entered(body: Node3D):
	if !_is_active:
		return
	
	if body is Item:
		var item: Item = body
		
		if item.get_collision_layer_value(CollisionLayers.LIGHT_ITEMS):
			return
	
	transition_to_state.emit(IdleState.ID, {})
	_deactivate()

func _physics_process_state(_delta: float):
	if _item.sleeping:
		transition_to_state.emit(IdleState.ID, {})
		_deactivate()
