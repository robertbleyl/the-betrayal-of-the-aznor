extends StateMachine.State
class_name CaughtState

const ID = "CAUGHT"

var _item: Item
var _player_character: PlayerCharacter

func _get_id() -> String:
	return ID

func _activate(data: Dictionary):
	super._activate(data)
	
	_item = get_parent().get_parent()
	_item.linear_velocity = Vector3.ZERO
	
	_player_character = data["player_character"]
	_item.set_collision_layer_value(CollisionLayers.FETCHING_ITEMS, false)
	_item.set_collision_layer_value(CollisionLayers.CAUGHT_ITEMS, true)
	_item.set_collision_mask_value(CollisionLayers.ENEMIES, false)
	
	call_deferred("_add_item_to_player_character")

func _add_item_to_player_character():
	_item.freeze = true
	_item.get_parent().remove_child(_item)
	_player_character.add_child(_item)
