extends StateMachine.State
class_name IdleState

const ID = "IDLE"

@export var freeze_cooldown: float = 1

var _item: Item
var _collision_layer: int
var _collision_mask: int
var _frozen: bool

var _freeze_timer: float

func _get_id() -> String:
	return ID

func _ready():
	_item = get_parent().get_parent()
	_frozen = _item.freeze
	_collision_layer = _item.collision_layer
	_collision_mask = _item.collision_mask

func _activate(_data: Dictionary):
	super._activate(_data)
	_item.collision_layer = _collision_layer
	_item.collision_mask = _collision_mask

func _physics_process_state(delta: float):
	if _frozen == _item.freeze:
		return
	
	_freeze_timer += delta
	if _freeze_timer >= freeze_cooldown:
		_item.freeze = _frozen
	
