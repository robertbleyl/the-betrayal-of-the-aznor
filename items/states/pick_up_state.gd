extends StateMachine.State
class_name PickUpState

const ID = "PICK_UP"

@onready var sound: DisposableAudioEmitter = %PickUpSound

@export var duration: float = 0.1
@export var wiggle_amount: float = 1.5

var _timer: float
var _data: Dictionary

func _get_id() -> String:
	return ID

func _activate(data: Dictionary):
	super._activate(data)
	_timer = duration
	_data = data
	
	sound.play_copy()

func _process_state(delta: float):
	_timer -= delta
		
	if _timer <= 0:
		transition_to_state.emit(FetchState.ID, _data)
		_deactivate()
