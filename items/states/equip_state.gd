extends StateMachine.State
class_name EquipState

const ID = "EQUIP"

var _item: Item
var _character: Node3D

func _get_id() -> String:
	return ID

func _activate(data: Dictionary):
	super._activate(data)
	
	_item = get_parent().get_parent()
	_item.linear_velocity = Vector3.ZERO
	
	_character = data["character"]
	_item.set_collision_layer_value(CollisionLayers.CAUGHT_ITEMS, false)
	_item.set_collision_mask_value(CollisionLayers.ENEMIES, false)
	
	call_deferred("_add_item_to_player_character")

func _add_item_to_player_character():
	_item.freeze = true
	_item.get_parent().remove_child(_item)
	_character.add_child(_item)
	
	#sprite_ground.visible = false
	#sprite_equipped.visible = true

func _deactivate():
	super._deactivate()
	#sprite_ground.visible = true
	#sprite_equipped.visible = false
