extends Item
class_name Weapon

@export var fire_cooldown: float = 1.0
@export var projectile_scene: PackedScene
@export var max_ammo: int = 20

@onready var fire_sound: DisposableAudioEmitter = %FireSound

var _fire_timer: float
var _current_ammo: int

func _ready():
	_current_ammo = max_ammo

func equip(character: Node3D):
	var data: Dictionary = {}
	data["character"] = character
	state_machine.transition_to_state(EquipState.ID, data)

func fire(target_position: Vector3):
	if _fire_timer > 0 || _current_ammo <= 0:
		return
	
	_fire_timer = fire_cooldown
	_current_ammo -= 1
	
	fire_sound.play_copy()
	
	var projectile: ProjectileBase = projectile_scene.instantiate()

	var character: RigidBody3D = get_parent()
	
	var character_layer = CollisionLayers.ENEMIES
	if character.get_collision_layer_value(CollisionLayers.ENEMIES):
		character_layer = CollisionLayers.PLAYER
	projectile.set_collision_mask_value(character_layer, true)
	
	character.get_parent().add_child(projectile)
	projectile.global_position = character.global_position
	projectile.apply_central_impulse((target_position - global_position).normalized() * projectile.speed)

func _process(delta: float):
	if _fire_timer > 0:
		_fire_timer -= delta
