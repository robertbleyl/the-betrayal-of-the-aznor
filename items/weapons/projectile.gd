extends RigidBody3D
class_name ProjectileBase

@export var speed: float = 7.0
@export var damage: float = 10.0
@export var damage_to_items: float = 1.0
@export var life_time: float = 3.0

var direction: Vector3

var _life_timer: float

func _process(delta: float):
	_life_timer += delta
	
	if _life_timer >= life_time:
		queue_free()

func _on_body_entered(body: Node3D):
	if body.has_method("receive_damage"):
		var dmg = damage_to_items if body is Item else damage
		var event: DamageEvent = DamageEvent.new()
		event.damage_amount = dmg
		event.direction = body.global_position - global_position
		event.collider = self
		body.receive_damage(event)
		
	queue_free()
