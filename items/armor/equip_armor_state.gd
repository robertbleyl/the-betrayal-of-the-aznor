extends StateMachine.State
class_name EquipArmorState

const ID = "EQUIP_ARMOR"

var _item: Item

func _get_id() -> String:
	return ID

func _activate(_data: Dictionary):
	super._activate(_data)
	
	_item = get_parent().get_parent()
	_item.freeze = true
	_item.set_collision_layer_value(CollisionLayers.LIGHT_ITEMS, false)
	_item.set_collision_layer_value(CollisionLayers.HEAVY_ITEMS, false)
	_item.set_collision_mask_value(CollisionLayers.PLAYER, false)
	_item.set_collision_mask_value(CollisionLayers.LIGHT_ITEMS, false)
	_item.set_collision_mask_value(CollisionLayers.HEAVY_ITEMS, false)
	_item.set_collision_mask_value(CollisionLayers.ENEMIES, false)
