extends Item
class_name Armor

func _ready():
	# We need to init this state in here because we have to make sure that the 
	# IdleState is initialized properly with the default collision mask values.
	state_machine.transition_to_state(EquipArmorState.ID, {})
