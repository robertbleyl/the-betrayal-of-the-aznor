extends FmodEventEmitter3D
class_name DisposableAudioEmitter

var _was_played = false

func play_copy():
	var copy = duplicate()
	get_parent().get_parent().get_parent().add_child(copy)
	copy.play()

func _set_played():
	_was_played = true

func _remove_after_sound_finished():
	if _was_played:
		queue_free()
